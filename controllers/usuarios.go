package controllers

import (
	"fmt"
	"github.com/hermescanuto/usuarios/models"
	"github.com/labstack/echo"
	"net/http"
	"strconv"
)


func init(){
	fmt.Println("Antes de tudo")
}

func Home(c echo.Context) error {


	var usuarios []models.Usuarios

	if err := models.UsuarioModel.Find().All(&usuarios); err != nil {
		return c.JSON(http.StatusBadRequest,
			map[string]string{
				"mensagem": "Erro ao tentar recuperar os dados!",
				"erro":     err.Error(),
			})
	}
	data := map[string]interface{}{
		"titulo": "Listagem de Usuário",
		"lista":  usuarios,
	}
	return c.Render(http.StatusOK, "index.html", data)
}

func Inserir(c echo.Context) error {

	nome := c.FormValue("nome")
	email := c.FormValue("email")

	var Usuario models.Usuarios
	Usuario.Nome = nome
	Usuario.Email = email

	if nome != "" && email != "" {
		if _, err := models.UsuarioModel.Insert(Usuario); err != nil {
			return c.JSON(http.StatusBadRequest,
				map[string]string{"mensagem": "Não foi possivel adcionar o registro no banco!"})
		}
		return c.JSON(http.StatusCreated,
			map[string]string{"mensagem": "Registro foi criado com sucesso!"})
	}
	return c.JSON(http.StatusBadRequest,
		map[string]string{"mensagem": "Os campos são obrigatório!"})
}

func Get(c echo.Context) error {
	data := map[string]interface{}{
		"titulo":  "Listagem de Usuário",
		"Content": "Content Hellow World",
	}
	return c.Render(http.StatusOK, "index.html", data)
}

func Deletar(c echo.Context) error {

	fmt.Println( "-------------->>> ", c.Request().Header.Get("token") )

	usuarioID, _ := strconv.Atoi(c.Param("id"))
	resultado := models.UsuarioModel.Find("id=?", usuarioID)

	if count, _ := resultado.Count(); count == 1 {
		if err := resultado.Delete(); err != nil {
			return c.JSON(http.StatusBadRequest,
				map[string]string{"mensagem": "Não foi possivel deletar o resultado"})
		}
		return c.JSON(http.StatusAccepted,
			map[string]string{"mensagem": "Usuario  Deletado com sucesso!"})
	}
	return c.JSON(http.StatusBadRequest,
		map[string]string{"mensagem": "Registro não foi deletado"})

}
