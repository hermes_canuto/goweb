package lib

import (
	"fmt"
	"log"
     config "github.com/hermescanuto/goconfigfromjsonfile"
	"upper.io/db.v3"
	"upper.io/db.v3/mysql"
)

var c = config.Getconfig()

//var configuracao = mysql.ConnectionURL{Host: "192.168.0.200", User: "root", Password: "131970", Database: "test"}

var configuracao = mysql.ConnectionURL{Host: c.Host, User: c.User, Password: c.Password, Database: c.Database}

// variavel que faz a conexao com o banco de dados
var Sess db.Database

func init() {
	var err error
	Sess, err = mysql.Open(configuracao)
	if err != nil {
		fmt.Println(configuracao)
		log.Fatal(err.Error())
	}
	Sess.SetLogging(true)
}
