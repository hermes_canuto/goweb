package main

import (
	config "github.com/hermescanuto/goconfigfromjsonfile"

	"github.com/MarcusMann/pongor-echo"
	r "github.com/hermescanuto/usuarios/routers"
	"github.com/labstack/echo/middleware"
)

func main() {
	e := r.App
	defer e.Close()

	p := pongor.GetRenderer(pongor.PongorOption{Reload: true})
	p.Directory = "views"
	e.Renderer = p

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"}}))

	e.Use(middleware.Logger())
	e.Logger.Fatal(e.Start(config.Getconfig().Server))

}
