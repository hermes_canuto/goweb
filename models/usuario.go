package models

import "github.com/hermescanuto/usuarios/lib"

// Usuarios representa a tabela usuario no banco de dados
type Usuarios struct {
	ID    int    `db:"id" json:"id"`
	Nome  string `db:"nome" json:"nome"`
	Email string `db:"email" json:"email"`
}

// usuario model recebe a tabela de banco de dados
var UsuarioModel = lib.Sess.Collection("usuarios")
