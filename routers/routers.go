package routers

import (
	"github.com/hermescanuto/usuarios/controllers"
	"github.com/labstack/echo"
)

// App é uma instancia de echo
var App *echo.Echo

func init() {
	App = echo.New()

	//Pagina inicial
	App.GET("/", controllers.Home)

	api := App.Group("/v1")

	api.POST("/insert", controllers.Inserir)
	api.GET("/user", controllers.Get)
	api.DELETE("/delete/:id", controllers.Deletar)

}
